# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Game, Player, Card, Neighborhood, Round
import random
import string


def start(request):
    return render(request, "start.html")


def creategame(request):
    newgame = Game.objects.create()
    randomid = ''.join(random.choice(string.lowercase) for i in range(10))
    newgame.gameid = randomid
    newgame.save()

    context_dict = ({"gameid": randomid})
    response = render(request, "creategame.html", context_dict)
    response.set_cookie(key="gameid", value=randomid)
    return response


def joingame(request, requestgameid):
    requestplayername = request.POST.get('playername')
    requesthood = request.POST.get('hoodname')

    if requestplayername is None:
        return render(request, "jointemplate.html", ({"gameid": requestgameid}))

    game = Game.objects.get(gameid=requestgameid)

    if game.playerone is None:
        game.playerone = initializeplayer(requestplayername, requesthood)
        game.save()

    elif game.playertwo is None:
        game.playertwo = initializeplayer(requestplayername, requesthood)
        game.save()
        return HttpResponseRedirect("/engine/showcards/")

    return render(request, "jointemplate.html", ({"gameid": requestgameid}))


def showcards(request):
    gameid = request.COOKIES["gameid"]

    game = Game.objects.get(gameid=gameid)

    rounds = [game.roundone, game.roundtwo, game.roundthree, game.roundfour, game.roundfive]

    for i in range(0, 5):
        if rounds[i] is not None and roundopen(rounds[i]):
            if rounds[i].playeronecard is None:
                player = game.playerone

            else:
                player = game.playertwo

            cards = getplayercards(player)

            if len(cards) == 0:
                return render(request, "finale.html", ({"game": game}))

            return render(request, "showcards.html", ({"cards": cards, "player": player}))

        else:
            newround = Round.objects.create()
            newround.save()

            if i == 0:
                game.roundone = newround
            elif i == 1:
                game.roundtwo = newround
            elif i == 2:
                game.roundthree = newround
            elif i == 3:
                game.roundfour = newround
            else:
                game.roundfive = newround

            game.save()

            player = game.playerone
            cards = getplayercards(player)

            return render(request, "showcards.html", ({"cards": cards, "player": player}))


def getplayercards(player):
    cardlist = []

    if player.cardone is not None:
        cardlist.append(player.cardone)

    if player.cardtwo is not None:
        cardlist.append(player.cardtwo)

    if player.cardthree is not None:
        cardlist.append(player.cardthree)

    if player.cardfour is not None:
        cardlist.append(player.cardfour)

    if player.cardfive is not None:
        cardlist.append(player.cardfive)

    return cardlist


def roundopen(round):
    if round.playeronecard is None:
        return True

    if round.playertwocard is None:
        return True

    return False


def playcard(request):
    cardid = request.POST.get("cardid")
    gameid = request.COOKIES["gameid"]

    game = Game.objects.get(gameid=gameid)
    rounds = [game.roundone, game.roundtwo, game.roundthree, game.roundfour, game.roundfive]

    for i in range(0, 5):
        if rounds[i] is not None and roundopen(rounds[i]):
            print("round {} is open".format(i))
            if rounds[i].playeronecard is None:
                print("player ones card")
                card = Card.objects.get(id=cardid)

                rounds[i].playeronecard = card
                rounds[i].save()

                player = game.playerone
                removecardfromplayer(player, card)

                return HttpResponseRedirect("/engine/showcards")

            elif rounds[i].playertwocard is None:
                print("player twos card")
                card = Card.objects.get(id=cardid)

                rounds[i].playertwocard = card
                rounds[i].save()

                player = game.playertwo
                removecardfromplayer(player, card)

                [winnerplayer, winnercard] = determinewinner(rounds[i].playeronecard, rounds[i].playertwocard)
                print ("Winner player is {}".format(winnerplayer))
                if winnerplayer == 1:
                    winner = game.playerone
                else:
                    winner = game.playertwo

                rounds[i].winner = winner
                rounds[i].save()

                # return winner page
                return render(request, "winnerpage.html", ({"winner": winner, "card": card}))


def initializeplayer(requestplayername, hoodname):
    newplayer = Player.objects.create(playername=requestplayername)
    populatePlayerCards(newplayer, hoodname)
    return newplayer


def populatePlayerCards(player, requesthoodname):
    hoodobject = Neighborhood.objects.get(id=requesthoodname)

    hoodcards = Card.objects.all()
    cardsfromhood = hoodcards.filter(hood=hoodobject.id)

    randomcards = random.sample(cardsfromhood, 5)

    player.cardone = randomcards[0]
    player.cardtwo = randomcards[1]
    player.cardthree = randomcards[2]
    player.cardfour = randomcards[3]
    player.cardfive = randomcards[4]

    player.save()


def removecardfromplayer(player, card):
    if player.cardone == card:
        player.cardone = None
    elif player.cardtwo == card:
        player.cardtwo = None
    elif player.cardthree == card:
        player.cardthree = None
    elif player.cardfour == card:
        player.cardfour = None
    elif player.cardfive == card:
        player.cardfive = None
    else:
        print ("Card not found")

    player.save()


def determinewinner(p1card, p2card):
    print ("{}/{} vs {}/{}".format(p1card.deathcause.power, p1card.profession.defence, p2card.deathcause.power, p2card.profession.defence))
    if (p1card.deathcause.power - p2card.profession.defence) > (p2card.deathcause.power - p1card.profession.defence):
        print ("winner 1")
        return [1, p1card]
    else:
        print ("winner 2")
        return [2, p2card]
