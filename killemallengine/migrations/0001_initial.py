# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-30 11:36
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('citizenname', models.CharField(max_length=30)),
                ('address', models.CharField(max_length=100)),
                ('dateofbirth', models.DateField()),
                ('dateofdeath', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Disease',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('diseasename', models.CharField(max_length=30)),
                ('power', models.FloatField()),
                ('iconname', models.CharField(max_length=8)),
            ],
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gameid', models.CharField(max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Neighborhood',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hoodname', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('playername', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Profession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('professionname', models.CharField(max_length=30)),
                ('defence', models.FloatField()),
                ('iconname', models.CharField(max_length=8)),
            ],
        ),
        migrations.CreateModel(
            name='Round',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('playeronecard', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='round_playeronecard', to='killemallengine.Card')),
                ('playertwocard', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='round_playertwocard', to='killemallengine.Card')),
                ('winner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='killemallengine.Player')),
            ],
        ),
        migrations.AddField(
            model_name='game',
            name='playerone',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='game_playerone', to='killemallengine.Player'),
        ),
        migrations.AddField(
            model_name='game',
            name='playertwo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='game_playertwo', to='killemallengine.Player'),
        ),
        migrations.AddField(
            model_name='game',
            name='roundfive',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='game_roundfive', to='killemallengine.Round'),
        ),
        migrations.AddField(
            model_name='game',
            name='roundfour',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='game_roundfour', to='killemallengine.Round'),
        ),
        migrations.AddField(
            model_name='game',
            name='roundone',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='game_roundone', to='killemallengine.Round'),
        ),
        migrations.AddField(
            model_name='game',
            name='roundthree',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='game_roundthree', to='killemallengine.Round'),
        ),
        migrations.AddField(
            model_name='game',
            name='roundtwo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='game_roundtwo', to='killemallengine.Round'),
        ),
        migrations.AddField(
            model_name='card',
            name='deathcause',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='killemallengine.Disease'),
        ),
        migrations.AddField(
            model_name='card',
            name='hood',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='killemallengine.Neighborhood'),
        ),
        migrations.AddField(
            model_name='card',
            name='profession',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='killemallengine.Profession'),
        ),
    ]
