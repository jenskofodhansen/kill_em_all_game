# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-30 16:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('killemallengine', '0004_auto_20170930_1351'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='description',
            field=models.CharField(default=2, max_length=1000),
            preserve_default=False,
        ),
    ]
