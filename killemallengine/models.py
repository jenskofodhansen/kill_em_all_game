# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class Disease(models.Model):
    diseasename = models.CharField(max_length=30)
    power = models.FloatField()
    iconname = models.CharField(max_length=8)

    def __unicode__(self):
        return self.diseasename


class Profession(models.Model):
    professionname = models.CharField(max_length=30)
    defence = models.FloatField()
    iconname = models.CharField(max_length=8)

    def __unicode__(self):
        return self.professionname


class Neighborhood(models.Model):
    hoodname = models.CharField(max_length=30)

    def __unicode__(self):
        return self.hoodname


class Card(models.Model):
    citizenname = models.CharField(max_length=30)
    address = models.CharField(max_length=100)
    deathcause = models.ForeignKey(Disease)
    profession = models.ForeignKey(Profession)
    dateofbirth = models.CharField(max_length=4)
    dateofdeath = models.CharField(max_length=4)
    hood = models.ForeignKey(Neighborhood)
    description = models.CharField(max_length=1000)
    imagename = models.CharField(max_length=40, default="billede")
    funfact = models.CharField(max_length=1000, default="fun fact")

    def __unicode__(self):
        return self.citizenname


class Player(models.Model):
    playername = models.CharField(max_length=30)
    cardone = models.ForeignKey(Card, blank=True, null=True, related_name="player_cardone")
    cardtwo = models.ForeignKey(Card, blank=True, null=True, related_name="player_cardtwo")
    cardthree = models.ForeignKey(Card, blank=True, null=True, related_name="player_cardthree")
    cardfour = models.ForeignKey(Card, blank=True, null=True, related_name="player_cardfour")
    cardfive = models.ForeignKey(Card, blank=True, null=True, related_name="player_cardfive")


class Round(models.Model):
    playeronecard = models.ForeignKey(Card, blank=True, null=True, related_name="round_playeronecard")
    playertwocard = models.ForeignKey(Card, blank=True, null=True, related_name="round_playertwocard")
    winner = models.ForeignKey(Player, blank=True, null=True)


class Game(models.Model):
    gameid = models.CharField(max_length=10)
    playerone = models.ForeignKey(Player, blank=True, null=True, related_name="game_playerone")
    playertwo = models.ForeignKey(Player, blank=True, null=True, related_name="game_playertwo")
    roundone = models.ForeignKey(Round, blank=True, null=True, related_name="game_roundone")
    roundtwo = models.ForeignKey(Round, blank=True, null=True, related_name="game_roundtwo")
    roundthree = models.ForeignKey(Round, blank=True, null=True, related_name="game_roundthree")
    roundfour = models.ForeignKey(Round, blank=True, null=True, related_name="game_roundfour")
    roundfive = models.ForeignKey(Round, blank=True, null=True, related_name="game_roundfive")
