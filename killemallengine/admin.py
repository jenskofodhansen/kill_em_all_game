# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from models import Disease, Profession, Card, Neighborhood

admin.site.register(Disease)
admin.site.register(Profession)
admin.site.register(Card)
admin.site.register(Neighborhood)
