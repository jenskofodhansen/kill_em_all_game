from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.start, name='start'),
    url(r'^creategame$', views.creategame, name='creategame'),
    url(r'^joingame/([a-z]+)/$', views.joingame, name='joingame'),
    url(r'^showcards', views.showcards, name='showcards'),
    url(r'^playcard', views.playcard, name='playcard'),
]
