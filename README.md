# README #

Welcome to the "Kill ' all game!" made for Hack4DK 2017

### What is this repository for? ###

* Quick summary
A small card game made for Hack4DK 2017 @ Enigma Museet, Copenhagen
Participants: 
Jens K Hansen - Backend developer
Svitlena - Frontend developer
Line - Story telling
Ole Krogskorg - Data crunching

* Version
First and probably final demo

### How do I get set up? ###

* Summary of set up
Clone the project and run _python manage.py runserver_

* Configuration
Runs on Python 2.7 with Django 1.11

* Database configuration
nothing, just the db.sqlite3

* How to run tests
It's a hackaton! No tests!


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact